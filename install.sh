#!/bin/bash

KISPHP_KISPHP_DIRECTORY="${HOME}/.kisphp_tool"
FG_RED="\033[31m"
FG_GREEN="\033[32m"
NC="\033[0m"

# todo check for dependencies
check_dependencies () {
    # check if bc is installed
    # check if git is installed
    return 0
}

clone_repo () {
    # full command to clone the project and save it into the
    CLONE_COMMAND="git clone https://gitlab.com/kisphp/bash-tool.git ${KISPHP_KISPHP_DIRECTORY}"

    # execute clone command
    $CLONE_COMMAND

    if [[ $? != 0 ]]; then
        echo -e "${FG_RED}Could not download the repository${NC}"
        return 1
    fi

    echo -e "${FG_GREEN}Successfully cloned repository${NC}"
    return 0
}

create_symlink () {
    if [[ -f /usr/local/bin/kp ]];then
        sudo rm /usr/local/bin/kp
    fi
    if [[ ! -f "${KISPHP_KISPHP_DIRECTORY}/kp.sh" ]];then
        echo -e "${FG_RED}Executable file does not exists: ${KISPHP_KISPHP_DIRECTORY}${NC}"
    fi
    sudo ln -s "${KISPHP_KISPHP_DIRECTORY}/kp.sh" /usr/local/bin/kp

    if [[ $? == 0 ]]; then
        echo -e "${FG_GREEN}Successfully created symlink${NC}"
        return 0
    else
        echo -e "${FG_RED}Could not create symlink${NC}"
    fi
}

check_dependencies && clone_repo && create_symlink

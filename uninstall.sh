#!/usr/bin/env bash

KISPHP_KISPHP_DIRECTORY="${HOME}/.kisphp_tool"

cd "${HOME}"

# remove symlink
sudo rm /usr/local/bin/kp

# remove directory
rm -rf "${KISPHP_KISPHP_DIRECTORY}"

# remove .kprc file
rm "${HOME}/.kprc"

#!/usr/bin/env bash

postgres () {

    FIRST_PARAM=''

    if [[ $# -gt 0 ]]; then
        FIRST_PARAM="${1}"
    fi

    if [[ "${FIRST_PARAM}" == 'admin' ]];then
        _postgres_run_adminer
    else
         _postgres_run_container
    fi
}

_postgres_run_container () {
    docker run \
        --name docker_postgres \
        -e POSTGRES_PASSWORD=kisphp \
        -e POSTGRES_DB=kisphp \
        -p "5432:5432" \
        postgres:10
}

_postgres_run_adminer () {
    docker run \
        -p "8880:80" \
        -e "PGADMIN_DEFAULT_EMAIL=admin@example.com" \
        -e "PGADMIN_DEFAULT_PASSWORD=kisphp" \
        --link docker_postgres \
        dpage/pgadmin4
}

postgres_help () {
    pre_help "postgres" "Run postgres inside a docker container"
    pre_help "postgres admin" "Run adminer to connect to postgres inside a docker container. View at localhost:8880. Connect with admin@example.com and pass: kisphp"
}

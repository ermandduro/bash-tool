#!/usr/bin/env bash

SNIFFER_ERROR_OUTPUT='/tmp/kisphp-sniffer-error.log'
BRANCH='master'
EDITED_FILE='/tmp/kisphp.edited.log'
EDITED_FILE_TRANSLATE='/tmp/kisphp.edited.tr.log'

edited_help () {
    HIDE='true'
    # echo "edited"
}

color_by_extension () {
    echo "$1--" | set "s/sh/bax/g"
    return 0
}

read_file () {
    while read -r var
    do
        echo -e "${var}"
    done < "${1}"
}

edited () {
    while getopts ":b:" opt; do
        case $opt in
            b)
                shift
                if [[ "${1}" != "" ]];then
                    BRANCH="${1}"
                fi
                ;;
        esac
    done

    files=$($GIT diff --name-only origin/$BRANCH 2>&1)

    if [[ $? != 0 ]]; then
        echo -e "${FG_RED}Branch ${BRANCH} does not exists${NC}"
        return 1
    fi

    for file in $files;
    do
        echo "${file}" >> $EDITED_FILE
    done

    local SED_RED='\\033\[31m'
    local SED_GREEN='\\033\[32m'
    local SED_YELLOW="\\033\[33m"
    local SED_BLUE="\\033\[34m"

    local SED_NC='\\033\[0m'

    # colorize php files
    sed "s/\.php/${SED_RED}\.php${SED_NC}/g" $EDITED_FILE > ${EDITED_FILE_TRANSLATE}
    mv "${EDITED_FILE_TRANSLATE}" "${EDITED_FILE}"

    # colorize twig files
    sed "s/\.twig/${SED_GREEN}\.twig${SED_NC}/g" $EDITED_FILE > ${EDITED_FILE_TRANSLATE}
    mv "${EDITED_FILE_TRANSLATE}" "${EDITED_FILE}"

    # colorize scss files
    sed "s/\.scss/${SED_BLUE}\.scss${SED_NC}/g" $EDITED_FILE > ${EDITED_FILE_TRANSLATE}
    mv "${EDITED_FILE_TRANSLATE}" "${EDITED_FILE}"

    # colorize js files
    sed "s/\.js/${SED_YELLOW}\.js${SED_NC}/g" $EDITED_FILE > ${EDITED_FILE_TRANSLATE}
    mv "${EDITED_FILE_TRANSLATE}" "${EDITED_FILE}"

    read_file "${EDITED_FILE}"

    rm $EDITED_FILE

    return 0
}

#!/usr/bin/env bash

kibana () {
    docker run \
        --rm \
        --name kibana \
        -p "5601:5601" \
        -u 0 \
        -it registry.gitlab.com/marius-rizac/ci-registry/kibana:latest
}

kibana_help () {
    pre_help "kibana (kb)" "Run kibana inside docker container"
}

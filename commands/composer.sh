#!/usr/bin/env bash

composer () {
    detect_linux_root

    ARGUMENTS="$@"
    docker run \
        --rm \
        -v `pwd`:/project \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -w /project \
        -it registry.gitlab.com/marius-rizac/ci-registry/php7.1:latest \
        sh -c "/bin/composer ${ARGUMENTS}"
}

composer_help () {
    pre_help "composer (c)" "Run composer inside docker container"
}

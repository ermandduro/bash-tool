#!/usr/bin/env bash

npm () {
    detect_linux_root
    echo -e "\nPlease run one of the following commands:"
    echo -e "\tkp npm3 [arguments]\t-->\tnodejs:6.14.3\tnpm:3.10.10"
    echo -e "\tkp npm5 [arguments]\t-->\tnodejs:8.11.3\tnpm:5.6.0"
    echo -e "\tkp npm6 [arguments]\t-->\tnodejs:10.7.0\tnpm:6.1.0"
}

npm3 () {
    detect_linux_root
    ARGUMENTS="$@"
    COMMAND="/usr/local/bin/npm ${ARGUMENTS}"
    if [[ "${1}" == "v" ]]; then
        COMMAND='echo -e "NodeJS: $(/usr/local/bin/node --version)\nNPM: $(/usr/local/bin/npm --version)"'
    fi
    kpLabelText "Running NPM 3"
    docker run \
        --rm \
        -v `pwd`:/project \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -w /project \
        -it registry.gitlab.com/marius-rizac/ci-registry/npm3:latest \
        bash -c "${COMMAND}"
}

npm5 () {
    detect_linux_root
    ARGUMENTS="$@"
    COMMAND="/usr/local/bin/npm ${ARGUMENTS}"
    if [[ "${1}" == "v" ]]; then
        COMMAND='echo -e "NodeJS: $(/usr/local/bin/node --version)\nNPM: $(/usr/local/bin/npm --version)"'
    fi
    kpLabelText "Running NPM 5"
    docker run \
        --rm \
        -v `pwd`:/project \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -w /project \
        -it registry.gitlab.com/marius-rizac/ci-registry/npm5:latest \
        bash -c "${COMMAND}"
}

npm6 () {
    detect_linux_root
    ARGUMENTS="$@"
    COMMAND="/usr/local/bin/npm ${ARGUMENTS}"
    if [[ "${1}" == "v" ]]; then
        COMMAND='echo -e "NodeJS: $(/usr/local/bin/node --version)\nNPM: $(/usr/local/bin/npm --version)"'
    fi
    kpLabelText "Running NPM 6"
    docker run \
        --rm \
        -v `pwd`:/project \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -w /project \
        -it registry.gitlab.com/marius-rizac/ci-registry/npm6:latest \
        bash -c "${COMMAND}"
}

npm_help () {
    pre_help "npm3 [arguments]" "Run Node 6 and NPM 3 inside docker container"
    pre_help "npm5 [arguments]" "Run Node 8 and NPM 5 inside docker container"
    pre_help "npm6 [arguments]" "Run Node 10 and NPM 6 inside docker container"
}

#!/usr/bin/env bash

KISPHP_DM_PORT=8080

docker_manager () {
    FIRST_PARAM=''

    if [[ $# -gt 0 ]]; then
        FIRST_PARAM="${1}"
        if [[ "${FIRST_PARAM}" == '-p' ]]; then
            if [[ "${2}" != '' ]]; then
                KISPHP_DM_PORT=$2
            fi
        fi
    fi

    if [[ "${FIRST_PARAM}" == 'up' ]]; then
        docker_manager_update
    else
        docker_manager_install
        docker_manager_run
    fi
}

docker_manager_install () {
    if [[ ! -d ~/.docker_manager ]]; then
        kpLabelText "Clone Docker manager"
        git clone https://gitlab.com/marius-rizac/docker-manager.git ~/.docker_manager
        kpLabelText "Run composer install"
        docker run \
            --rm \
            -v ~/.docker_manager:/project \
            -v ~/.ssh:/root/.ssh \
            -v ~/.composer:/root/.composer \
            -w /project \
            -it registry.gitlab.com/marius-rizac/ci-registry/php7.1:latest \
            sh -c "./build.sh dev"
    fi
}

docker_manager_run () {
    kpSuccessText "Docker Manager is available at http://localhost:${KISPHP_DM_PORT}"
    php -S "localhost:${KISPHP_DM_PORT}" -t ~/.docker_manager/web
}

# @todo add update option for docker manager tool
docker_manager_update () {
    if [[ -d ~/.docker_manager ]]; then
        kpLabelText "update docker manager"
        cd ~/.docker_manager/ && git pull && cd -
    fi
}

docker_manager_help () {
    pre_help "dm [-p 1234]" "Run docker manager"
    pre_help "dm [up]" "Update Docker manager"
}

#!/usr/bin/env bash

pgadmin () {
    if [[ $# -gt 0 ]]; then
        if [[ "${1}" == 'off' ]]; then
            pgadmin_off
            exit 0
        fi
    fi

    pgadmin_on
    exit 0
}

pgadmin_on () {
    docker run \
        --rm \
        -d \
        --name pgadmin_4_docker \
        -p "8484:80" \
        -e "PGADMIN_DEFAULT_EMAIL=admin@example.com" \
        -e "PGADMIN_DEFAULT_PASSWORD=kisphp" \
        dpage/pgadmin4 \
    && open http://localhost:8484
}

pgadmin_off () {
    docker stop pgadmin_4_docker
}

pgadmin_help () {
    pre_help "pgadmin (pg)" "Run PgAdmin 4 in container. http://localhost:8484"
    pre_help "pgadmin (pg) off" "Stop PgAdmin 4 container"
}

#!/usr/bin/env bash

redis () {

    # A POSIX variable
    OPTIND=1 # Reset in case getopts has been used previously in the shell.

    # Initialize our own variables:
    hostname="127.0.0.1"
    port=6379

    while getopts "h:p:" opt; do
        case "$opt" in
        h)
            hostname=$OPTARG
            ;;
        p)
            port=$OPTARG
            ;;
        esac
    done

    shift $((OPTIND-1))

    [ "${1:-}" = "--" ] && shift

    docker run \
        --rm \
        --name redis-cli \
        -it goodsmileduck/redis-cli \
        sh -c "/usr/local/bin/redis-cli -h ${hostname} -p ${port} $@"
}

redis_help () {
    pre_help "redis (rc)" "Run Redis-CLI inside docker container"
}

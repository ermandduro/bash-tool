#!/usr/bin/env bash

mysql () {
    if [[ "${1}" == 'admin' ]]; then
        _mysql_run_admin
    else
        _mysql_run_container
    fi
}

_mysql_run_container () {
    docker run \
        -p "3306:3306" \
        -e MYSQL_DATABASE=kisphp \
        -e MYSQL_ROOT_PASSWORD=kisphp \
        -e MYSQL_USER=kisphp \
        -e MYSQL_PASSWORD=kisphp \
        --name docker_mysql \
        -it registry.gitlab.com/marius-rizac/ci-registry/mysql5.7:latest
}

_mysql_run_admin () {
    docker run --rm \
        --name myadmin \
        -e PMA_HOST=docker_mysql \
        -p "8888:80" \
        --link docker_mysql:docker_mysql \
        phpmyadmin/phpmyadmin
}

mysql_help () {
    pre_help "mysql" "Run mysql 5.7 inside a docker container"
    pre_help "mysql admin" "Run phpmyadmin that will connect to mysql server. View at localhost:8888"
}

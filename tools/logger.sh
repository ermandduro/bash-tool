#!/usr/bin/env bash

kp_log () {
    if [[ "${KP_LOGS}" -gt 0 ]];then
        message=$1
        label=$2
        if [[ -z "${label}" ]];then
            label='Info'
        fi
        now=$(date +%Y-%m-%d\ %H:%M:%S)
        echo "${now} [${label}] ${message}" >> "${KISPHP_KISPHP_DIRECTORY}/logs/cmd.log"
    fi
}

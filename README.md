# Kisphp development tool helper

## Install

```bash
curl https://gitlab.com/kisphp/bash-tool/raw/master/install.sh | bash -
```

## Contribute

Run the following commands to install the application

```bash
git clone https://gitlab.com/kisphp/bash-tool.git kisphp-tool
cd kisphp-tool 
./install.sh
cd ../ 
rm -rf kisphp-tool
```
After installation you can run the tool: `kp`

Write `kp` in terminal and press enter to see the available commands:


#### Run linux commands in docker container
```bash
kp kisphp [command here]
# or
kp k [command here]

# example build script
kp k ./build.sh dev
```


#### Run composer commands inside docker container
```bash
kp composer {composer_subcommand}

# install
kp composer install
# or using shortcut
kp c install
# install without interaction
kp c install --no-interaction

# update
kp c update
```

#### Run Mysql container
```bash
kp mysql
# Credentials:
ROOT_PASSWORD  = kisphp
DATABASE       = kisphp
MYSQL_USER     = kisphp
MYSQL_PASSWORD = kisphp
```

#### Run phpMyadmin container
```bash
kp mysql admin
```

> phpmyadmin will be available at [http://localhost:8888](http://localhost:8888)


#### Run Postgres container
```bash
kp postgres

# Credentials
POSTGRES_HOSTNAME = docker_postgres
POSTGRES_PASSWORD = kisphp
POSTGRES_DB       = kisphp
POSTGRES_PORT     = 5432
```

#### Run pgAdmin 4
```bash
kp postgres admin

# Credentials
PGADMIN_DEFAULT_EMAIL    = admin@example.com
PGADMIN_DEFAULT_PASSWORD = kisphp
```

> pgadmin 4 will be available at [http://localhost:8880](http://localhost:8880)

#### Run Redis CLI
```bash
kp redis

# or using shortcut
kp rc
# connect to remote host
kp rc -h 80.80.80.80
# connect to different port
kp rc -p 12345
# connect to remote host and different port
kp rc -h 123.123.123.123 -p 12345
```

> You also can pass more redis-cli arguments to this command

#### Run npm commands inside docker container

Run `nodejs:6.14.3 npm:3.10.10`

```bash
# see available scripts
kp npm3 run

# run npm install
kp npm3 install
```

Run `nodejs:8.11.3 npm:5.6.0`

```bash
# see available scripts
kp npm5 run

# run npm install
kp npm5 install
```

Run `nodejs:10.7.0 npm:6.1.0`

```bash
# see available scripts
kp npm6 run

# run npm install
kp npm6 install
```

#### View edited files in current branch
```bash
kp edited

# or shortcuts:
kp e
kp edt

# or specify a different branch to check differences (default is master):
kp edited -b other-feature
```

#### Run Kibana docker container
```bash
kp kibana
# or
kp kb
```

> After container starts, you can open this url in browser [http://localhost:5601](http://localhost:5601)


#### Upgrade tool
```bash
kp upgrade

# or shortcut
kp up
```

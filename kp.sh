#!/usr/bin/env bash

# get error checking for free
#set -o errexit
# exit if trying to use uninitialised variables
#set -o nounset

GIT=$(which git)
PHP=$(which php)
CURRENT_DIR=$(pwd)
COMPOSER=$(which composer)

# get filename without full path
TOOL_NAME="${0##*/}"

export KISPHP_KISPHP_DIRECTORY="${HOME}/.kisphp_tool"

# create config files in user's home directory if does not exists
if [[ ! -f $HOME/.kprc ]]; then
    cp "${KISPHP_KISPHP_DIRECTORY}/.kprc.dist" "${HOME}/.kprc"
fi

# load config file
. "${HOME}/.kprc"

# load tools libraries
. "${KISPHP_KISPHP_DIRECTORY}/tools/helpers.sh"
. "${KISPHP_KISPHP_DIRECTORY}/tools/logger.sh"
. "${KISPHP_KISPHP_DIRECTORY}/tools/print.sh"

# load commands
for COMMAND in $(find "${KISPHP_KISPHP_DIRECTORY}/commands/" -type f -name '*.sh'); do
    . $COMMAND
done

parse_command () {
    if [[ $# -eq 0 ]];then
        usage
        _kisphp_tool_should_upgrade

        exit 1
    fi

    local command="${1}"
    case "${command}" in
        dm)
            shift
            docker_manager $@
            echo -e "\n"
            return 0
            ;;
        redis|rc)
            shift
            redis "$@"
            echo -e "\n"
            return 0
            ;;
        kibana|kb)
            shift
            kibana "$@"
            echo -e "\n"
            return 0
            ;;
        kisphp|k)
            shift
            kisphp "$@"
            echo -e "\n"
            return 0
            ;;
        composer|c)
            shift
            composer "$@"
            echo -e "\n"
            return 0
            ;;
        npm)
            shift
            npm "$@"
            echo -e "\n"
            return 0
            ;;
        npm3)
            shift
            npm3 "$@"
            echo -e "\n"
            return 0
            ;;
        npm5)
            shift
            npm5 "$@"
            echo -e "\n"
            return 0
            ;;
        npm6)
            shift
            npm6 "$@"
            echo -e "\n"
            return 0
            ;;
        postgres)
            shift
            postgres "$@"
            echo -e "\n"
            return 0
            ;;
        pgadmin|pg)
            shift
            pgadmin "$@"
            echo -e "\n"
            return 0
            ;;
        mysql)
            shift
            mysql "$@"
            echo -e "\n"
            return 0
            ;;
        edited|edt|e)
            shift
            edited "$@"
            echo -e "\n"
            return 0
            ;;
        upgrade|up)
            shift
            upgrade
            echo -e "\n"
            return 0
            ;;
    esac
}

pre_help () {
    LIMIT_CHARS=25
    CHARS=$(echo -n ${1} | wc -c)
    LEFT_CHARS=`expr $LIMIT_CHARS - $CHARS`
    SPACES=''
    for ((i=1; i<=$LEFT_CHARS; i++)); do
        SPACES="${SPACES}."
    done
    echo -e "\t${TOOL_NAME} ${1} ${SPACES} ${2}"
}

usage () {
    detect_linux_root
    echo "Usage: "
    for COMMAND in $(find "${KISPHP_KISPHP_DIRECTORY}/commands/" -type f -name '*.sh'); do
        COMMAND="${COMMAND##*/}"

        $(echo "${COMMAND%.sh}_help")
    done

    pre_help "uninstall" "Will uninstall this tool"
}

main () {
    parse_command "$@"

    _kisphp_tool_should_upgrade
}

main "$@"
